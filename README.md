# PIM Controller #

**Last updated:** 2/9/2017

**Creators:**
	Created by Camille Girabawe and Ian Hunter in the Fraden lab at Brandeis in order to control experimental complex chemical oscillator networks.
	
**Overview and goals:**
	A modular Python code for controlling programmable illumination devices (PIM) for scientific purposes. For an example of a PIM http://fraden.brandeis.edu/publications/papers/TompkinsAJP.pdf .
	The goal is for this code to allow systematic spatially, and temporally varying illumination of samples by experimentalists in the scientific community. Also this code will allow this to be
	done without experimentalists needing to spend months developing slow, unintuitive Matlab code, but instead get right to testing their theories about how light perturbations impact	
	system of choice. A focus within this code will be on allowing state variables to be defined based on the state of user defined regions of interest, then control routines to be defined to
	update the illumination of the sample based upon changes in these state variables. Also important is the fact that this code will be open source, with code any interested user can fine-tune	
	to their purpose. 
	
**Implementation and necessary equiptment for users:**
	Though there are many ways in principle to approach to creating such a code, we have taken a specific approach based upon requiring a central computer, 2 monitors, a projector, a camera, and
	light focusing lenses and objectives. Within this framework a micron scale sample is fixed in place, and viewed through Kohler illumation microscopy by a camera. This camera is then live feeds
	the sample to the computer. The computer can then deliver light pertubations to the sample via a projector connected to a microscope objective in reverse, focused at the sample. The projector,
	being the type in classrooms and offices, is controlled via a secondary monitor. Our code thus deliver light perturbations to the sample via sending a prospective pattern to a secondary monitor.
	This avoids the tricky issue of directly interfacing with projectors, at the low cost of a secondary monitor. Currently only linux and mac systems will be supported. 
	
**Installation:**
	First install all of the files in the .git repository
	Then following the instructions in 'dependenciesandinstall_'..',md', which vary whether you are installing this software on a Mac or Linux platform.
	
Currently only supports sample viewing through DCAM firewire. This will be expanded to NIKON DSLRs in the coming months (2/2017).
