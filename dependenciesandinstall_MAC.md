Last updated: 2/10/2017


Installation dependencies on MAC necessary for base PIM code:
	From terminal enter the following commands
	*Python 
		$brew install python 
	*Numpy 
		$brew install numpy 
	*Update pythonpath 
		$export PYTHONPATH=”/usr/local/bin:/usr/local/sbin:$PYTHONPATH” 
	*Install opencv 
		$brew tap homebrew/science 
		Have a look at other options 
		$brew info opencv 
		Now, you can install  
		$brew install opencv 
 
		Locate cv.py and cv2.so in the directory where opencv was installed and and their paths to the python doing the following: 
		Go to you pythonpath and run following commands 
		$ln -s /usr/local/Cellar/opencv/2.4.9/lib/python2.7/site-packages/cv.py cv.py 
		$ln -s /usr/local/Cellar/opencv/2.4.9/lib/python2.7/site-packages/cv2.so cv2.so 
	*Install PyQT 
		Make sure that homebrew and xcode and installed and updated 
		Clone/Download following PyQt and Sip repos: 
		$wget https://sourceforge.net/projects/pyqt/files/PyQt4/PyQt-4.11.4/PyQt-mac-gpl-4.11.4.tar.gz  
		$wget http://freefr.dl.sourceforge.net/project/pyqt/sip/sip-4.18/sip-4.18.tar.gz  
		Unzip downloaded files 
		$tar -xvf sip-4.18.tar.gz 
		$cd /sip-4.18  
		$python configure.py -d /usr/local/lib/python2.7/site-packages/ 
		$make 
		$make install 
		$cd .. 
		$tar -xvf PyQt-mac-gpl-4.11.4.tar.gz 
		$cd PyQt-mac-gpl-4.11.4 
		$python configure-ng.py -d /usr/local/lib/python2.7/site-packages/ --qmake=/opt/local/libexec/qt4/bin/qmake --sip=/usr/local/bin/sip --sip-incdir=../sip-4.18/siplib 
		$make 
		$make install 
 
		Note: on older Mac, you may need to install native qt. 
		$brew install qtX 
			where X stands for the version 

Camera dependent dependencies on MAC necessary for PIM:
	*Firewire cameras 
		**Install Pydc1394 
			$brew install libdc1394 
			$sudo pip install pydc1394 
	*NIKON DSLR cameras:
