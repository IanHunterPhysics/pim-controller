'''
    Programmable illumination microscope application
	Pixel transformation functions
    Authors:
        Camille Girabawe
        Ian Hunter

    Functionalities:
        - Accepts the 2d points defining the polygons which define 
	the regions of interests during a PIM session.
	- Can transform multiple ROIs at the same time, to iteratively 
	transform them into any shape necessary
    Last 3 updates:
        -(12/20/2016) Created and tested initially
        - N/A
        -N/A

    GIT_repo info:


'''
import numpy as np
from numpy import matrix as mat
from numpy import array as arr

import math
from math import cos
from math import sin
from math import pi

#define global indexing
xI=0
yI=1

#base transform matrices 
#the textbook definition implemented in python
#rotates about the origin, NOT the center of the vertex list it is called on
def rotmat(theta):
	return mat([[cos(theta),sin(theta)],[-1*sin(theta),cos(theta)]])
#a shear matrix as defined by wikipedia
#appears to work well in mathematica .nb file
def shearmat(sx,sy):
	SX=mat([[1,sx],[0,1]]) #x axis shear transform
	SY=mat([[1,0],[sy,1]]) #y axis shear transform
	#noting that the determinant of their product will also be
	#unit as they are units, and thus is also a valid shear mat
	#acting on x and y coords simeltaneously
	return mat([[1,sx],[sy,1]]) #pretty sure order doesn't really matter

def vertexlistunbiasedCOM(vlist): #vlist must be np arr or mat
	temp=np.mean(arr(vlist),axis=0)
	return temp


#full functions


#Translation
#define translation (net motion in space) per vertex mapping
def translatemap(vert,dx,dy):
	return arr([vert[0]+dx,vert[1]+dy])
#create vectorized function to allow mapping over np arrs
transmapvectorized = np.vectorize(translatemap)

#full function
def translate(vlist,dx,dy): 
	tmp=np.copy(vlist)
	for i in range(len(tmp)):
		tmp[i]=translatemap(tmp[i],dx,dy)
	return tmp

#Rescaling
#define rescale per vertex mapping
def rescalemap(vert,cx,cy):
	return arr([vert[xI]*cx,vert[yI]*cy])
rescalemapvectorized = np.vectorize(rescalemap)
def rescale(vlist,cx,cy):
	tmp=np.copy(vlist)
	com = vertexlistunbiasedCOM(tmp)
	vlistO = translate(tmp,-1*com[xI],-1*com[yI])
	for i in range(len(vlistO)):
		vlistO[i] =  rescalemap(vlistO[i],cx,cy)
	return translate(vlistO,com[xI],com[yI])

def shearmap(vert,shearmat):
	tmp = np.dot(shearmat,vert)
	return tmp

shearmapvectorized = np.vectorize(shearmap)

def unitaryrescale(vlist,sx,sy):
	tmp=np.copy(vlist)
	smat = shearmat(sx,sy)
	com = vertexlistunbiasedCOM(tmp)
	vlistO = translate(tmp,-1*com[xI],-1*com[yI]) #center about origin
	for i in range(len(vlistO)):
		vlistO[i] =  shearmap(vlistO[i],smat)
	tmp = np.copy(vlistO)
	return translate(tmp,com[xI],com[yI])

def rotatemap(vert,rmat):
	tmp =rmat.dot(vert)
	return tmp

rotmapvectorized = np.vectorize(rotatemap)
def rotate(vlist,theta):
	tmp=np.copy(vlist)
	#force polygon to be about the origin so rotation about origin is actually useful
	rmat = rotmat(theta)
	com = vertexlistunbiasedCOM(tmp)
	vlistO = translate(tmp,-1*com[xI],-1*com[yI]) #center about origin
	for i in range(len(vlistO)):
		vlistO[i] =  rotatemap(vlistO[i],rmat)
	temp= translate(vlistO,com[xI],com[yI])	
	return temp



