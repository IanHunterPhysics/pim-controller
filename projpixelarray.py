"""
    Programmable illumination microscope application
	Static projection routines
    Authors:
        Camille Girabawe
        Ian Hunter

    Functionalities:
        - Initialize a secondary monitor for projection
	- Project a static image of it from a large array of pixel values
    Last 3 updates:
        -~(11/23/2016) Static projection working
        -N/A
        -N/A

    GIT_repo info:


"""
import numpy
import pyglet
import sys
import os
import time
from numpy import array as arr
RGBMAX=255

class PIMWin(object):
    '''
    window data


    '''
    window = None   #pyglet window objection
    screeni = 0     #chosen screen
    winymax = 0     #height of window in pixels
    winyo = 0       #height of window start in pixels, seems useless but I may not understand it
    winxmax = 0     #width of window in pixels
    winxo = 0       #width of window start in pixels, seems useless but I may not understand it
    background_col=(0,0,0,1) #black background 


    '''
    frame update data
    
    '''

    RGBpxarr = None
    timeout = 0
    imxmax = 0
    imymax = 0
    dx = 0          #width of actual pixels per projection 'pixel'
    dy =0           #height of "                                 "
    '''
    error reporting data


    '''
    '''
    pixels
    '''
    batchpixels=None
    pixels=None

    def __init__(self,screeni,stdout):
       # print str(screeni)+' '+str(stdout)
        print 'initialization of PIM projection window begun'
        self.screeni=screeni
        platform = pyglet.window.get_platform()
        display = platform.get_default_display()
        screens = display.get_screens()
        if stdout:
            print screens
        screen = screens[screeni]
        window  = pyglet.window.Window(style='borderless')
        #window.set_size(screen.width,screen.height)
#        window.set_fullscreen(fullscreen=True,screen=screen,mode=None,width=screen.width,height=screen.height)
        window.set_fullscreen(fullscreen=False,screen=screen,mode=None,width=screen.width,height=screen.height)

        #window.set_location(int(screen.x/2.0),int(screen.y/2.0))
        window.set_mouse_visible(False)
        window.set_exclusive_mouse(False)
        self.window=window

        self.winymax=screen.height
        self.winxmax=screen.width
        self.winyo=screen.y
        self.winxo=screen.x
        pyglet.gl.glClearColor(*self.background_col)
        window.clear() #set to black by default, will be white otherwise
        window.set_location(self.winxo,self.winyo)
        #window.maximize()
        #self.window.set_size(self.winxmax,self.winymax)
        print 'initialization of PIM projection window complete'
        
    def project_void(self):
        blackpx=[255,255,255,0]
        RGBpxarr=arr([[blackpx,blackpx],[blackpx,blackpx]])
        self.project_const(RGBpxarr,1.0)
        return

    def project_const(self,RGBpxarr,timeout):
        if self.batchpixels!=None:
            del self.batchpixels
            self.batchpixels=None
        if self.pixels!=None:
            del self.pixels
            self.pixels=None
        print 'static projection initiated'
        global killproj,PROJ_ERR_FLAG                                                                              
        (self.imymax,self.imxmax,pxvecD)=RGBpxarr.shape #number of rows, cols and dimensionality of px                                                                        
        if (float(self.imxmax)/float(self.imymax))!=float(self.winxmax)/float(self.winymax):
            PROJ_ERR_FLAG=1
        if pxvecD!=4:
            PROJ_ERR_FLAG=2
            return #as px vals not being RGB is a fatal error                                                                                                       
        self.dx = self.winxmax/float(self.imxmax)
        self.dy = self.winymax/float(self.imymax)
        self.batchpixels=pyglet.graphics.Batch()
        self.pixels=[]                                                                                                 
        for i in (range(self.imymax))[::-1]: # i in [n-1,n-2,....,0] so starts from bottom left                                                       
            for j in range(self.imxmax):
#                print str(i)+' '+str(j)
                Iij=RGBpxarr[i][j][3] # get the intensity value of the pixel                                                                                        
                normedcolij=( RGBpxarr[i][j][0]*Iij,RGBpxarr[i][j][1]*Iij,RGBpxarr[i][j][2]*Iij)#assumes RGB vals given between 0-255 each                          
                col0=int(normedcolij[0]);
                col1=int(normedcolij[1]);
                col2=int(normedcolij[2]);
                x,y=(self.dx*j),(self.dy*(self.imymax-(i+1)))
		#create pixels which are actually OPENGL rectangles
                self.pixels=self.batchpixels.add(4,pyglet.gl.GL_QUADS,None, ('v2f',[x,y,x+self.dx,y,x+self.dx,y+self.dy,x,y+self.dy]),('c3B',(col0,col1,col2,col0,col1,col2,col0,col1,col2,col0,col1,col2)))
#                print [x,y,x+self.dx,y,x+self.dx,y+self.dy,x,y+self.dy]
        @self.window.event
        def on_draw():
            #print 'in loop'
            self.batchpixels.draw()

        def exitafterdt(dt):
            print 'static projection now active'
           # pyglet.app.exit()
        #    print 'closed pyglet window (?)'
        #time.sleep(0.5)
        pyglet.clock.set_fps_limit(20)
        pyglet.clock.schedule_once(exitafterdt,timeout) #definitely necessary. otherwise only first projection shows.
        pyglet.app.run()
        print 'static projection now active'
        #print 'app running now'
        return 


    #def project_openloop(self,RGBpxarrdyn,timeout):
        






def main():
    print 'main entered'
    pass
if __name__ == "__main__":
    main()
