'''
    Programmable illumination microscope application

    Authors:
        Camille Girabawe
        Ian Hunter

    Functionalities:
        - [x] open and close video instance. Now you can open and close the camera as many times as possible
        - [-] snap a single frame from the video instance. There is a bug that doesn't allow to snap a frame while the camera is still open.
            * Try to close the camera before snapping and open it to snap (and if necessary, reclose it afterwards)
        - save a single or multiple frames from the video instance
        - select multiple regions of interest and save selected coordinates
        - project light on selected regions of interest
        - ability to change coordinates and light intensity of regions of interest
    Last 3 updates:
        -(12/23/2016) Projection working, initial camera failures debugged
        -N/A
        -N/A

    GIT_repo info:


'''

import sys, os, time, cv2
import datetime as dt
import numpy as np
from numpy import array as arr
import time
import optparse
from projpixelarray import PIMWin
import pydc1394 as pydc
from pydc1394 import DC1394Library, Camera
from pydc1394.ui.qt import LiveCameraWin
from pydc1394.cmdline import add_common_options, handle_common_options
from transforms_2d import *



try:
    from PyQt4 import QtGui, QtCore
except:
    from PyQt5 import QtGui, QtCore


#     GLOBAL VARIABLES:

#//Camera variables
MAXNUMCAMREOPEN=1 #iterations
CAMFORCEDWAIT=1.0 #seconds
CAMBUFFER=30 #num frames of buffer. This is the max number of frames which pydc1394 holds onto
#before failing. Needs to be non-zero as during heavy computation, such as during projection, 
#the camera will fall behind slightly. If you don't leave any frames in your wake, the camera
#and the viewing screen will go out of synch and fail (I wish they could have made it just lag not 
#fail. With time I could make this change.)
CAMINTERACTIVE=True #if you don't set this to True pydc1394 can push at most 1000 jobs on the
#python queue.py threading job. For some reason it will push these jobs basically without
#limit until it fails.  Setting it to True appears to completely resolve this issue. 

REPROJWAIT=0.1 #seconds, tested and works for upwards of 14 hours with no sign of failing

#//GUI variables


#//


class MyCamera(QtCore.QThread):#needs to be an external package; we may change cameras soon :|
    def __init__(self, cam, parent = None):
        super(MyCamera, self).__init__(parent)
        self._stopped = False
        self._mutex = QtCore.QMutex()
        
        self._cam = cam
        time.sleep(0.1)
        self.start()
        time.sleep(0.1)
        print "camera started"
    
    def stop(self):

        try:
            self._mutex.lock()
            self._stopped = True
        finally:
            self._mutex.unlock()

    def isStopped(self):
        s = False
        #print "stopping camera"
        try:
            self._mutex.lock()
            s = self._stopped
        finally:
            self._mutex.unlock()
            return s

    def run(self):
        if not self._cam.running:
            print 'starting new camera'
            #time.sleep(0.1)

            n = 0
            camloaded=False
            while n<MAXNUMCAMREOPEN and not camloaded:
                try:
                    self._cam.start(CAMBUFFER,interactive=CAMINTERACTIVE)#changed from True on 12/23. Seems to make it run slightly more reliably

                except RuntimeError:
                    print 'Runtime error occurred while starting cam. Will try to load the cam '+ str(MAXNUMCAMREOPEN-n)+' more times.'
                    time.sleep(CAMFORCEDWAIT)
                    #self.cam.stop()
                    #del self.cam

                else:
                    camloaded=True
                n=n+1
            if not camloaded:
                print 'Loading of camera totally failed. Program shutting down so you can restart it.'
                quit()
        
        while not self.isStopped():

            self._cam.new_image.acquire()
            if not self._cam.running:
                self.stop()
            else:
                time.sleep(REPROJWAIT)
                self.frame = np.array(self._cam.current_image)
               # print str(self._cam.current_image.frames_behind)+' frames behind'
                #perhaps draw frames until you reach the true current frame?


                height,width = self.frame.shape
                #print dt.datetime.now()
                im = QtGui.QImage(self.frame.repeat(3).tostring(), int(width), int(height),QtGui.QImage.Format_RGB888).rgbSwapped()
                self.emit(QtCore.SIGNAL("newImage"), im)
                cv2.imwrite('frame00.png',self.frame)
            self._cam.new_image.release()
    def keep_image_size(self,width,height):
        self.image_width = width
        self.image_height = height


class VideoPlayer(QtGui.QWidget):
    def __init__(self,cam=None):
        super(VideoPlayer,self).__init__()
        self.resize(720,540)
        self.move(600,20)
        self.setWindowTitle('Video Player')
        
        self.scene = QtGui.QGraphicsScene(self)
        self.view  = QtGui.QGraphicsView(self.scene)
        self.vbox = QtGui.QVBoxLayout()
        self.vbox.addWidget(self.view)
        
        self.framerate = cam.fps
        #print "frame rate:",str(self.framerate)
    #    try:
        self.myCam = MyCamera(cam)
  #      except libdc1394 warning:
 #           print 'libdc1394 warning error during start of cam in VideoPlayer'

        QtCore.QObject.connect(self.myCam, QtCore.SIGNAL("newImage"), self.processFrame)
        
        self.setLayout(self.vbox)
        self.show()
    
    def processFrame(self,im):
        pix = QtGui.QPixmap(im).scaled(640,480)#,QtCore.Qt.KeepAspectRatio
        self.scene.clear()
        self.scene.clear()
        self.scene.addPixmap(pix)
    
    def closeEvent(self,event):
        self.myCam.stop()
        print "camera stopped"


class MainWindow(QtGui.QWidget):
    def __init__(self):
        super(MainWindow,self).__init__()
        self.resize(1080,550)
        self.move(200,100)
        self.setWindowTitle('PIM App')
        
        self.__validate_files__()
        #variables
        self.num_rois = 20
        self.frame_counter = 0
        self.time_interval = 1
        self.vertices = []
        self.regions = []
        self.snapped_image = None
        
        #buttons
        self.btn_open_camera = QtGui.QPushButton('OpenCamera')
        self.btn_open_camera.clicked[bool].connect(self.open_camera)
        
        self.btn_add_roi = QtGui.QPushButton('AddROI')
        #self.btn_add_roi.setCheckable(True)
        self.btn_add_roi.clicked[bool].connect(self.add_roi)
        
        self.btn_transform_rois = QtGui.QPushButton('TransformROIs')
        self.btn_transform_rois.clicked[bool].connect(self.transform_rois)

        self.btn_reset_rois = QtGui.QPushButton('ResetROIs')
        self.btn_reset_rois.clicked[bool].connect(self.reset_rois)

        
        self.btn_project = QtGui.QPushButton('Project')
        self.btn_project.clicked[bool].connect(self.project)
        
        self.btn_snapshot = QtGui.QPushButton('SnapShot')
        self.btn_snapshot.clicked[bool].connect(self.snapshot)
        
        self.btn_save_image = QtGui.QPushButton('SaveImage')
        self.btn_save_image.clicked[bool].connect(self.save_snapped)
        
        self.btn_record = QtGui.QPushButton('Record')
        self.btn_record.setCheckable(True)
        self.btn_record.clicked[bool].connect(self.record)
        self._record_signal = False
        
        #table
        self.tbl = QtGui.QTableWidget(self.num_rois,3,self)
        self.tbl.setColumnWidth(0,75)
        self.tbl.setColumnWidth(1,75)
        self.tbl.setColumnWidth(2,75)
        self.tbl.setHorizontalHeaderLabels(["X","Y","I"])
        
        #create a layouts for buttons
        b_layout = QtGui.QGridLayout() #for buttons
        b_layout.addWidget(self.btn_open_camera,0,0)
        b_layout.addWidget(self.btn_snapshot,0,1)
        b_layout.addWidget(self.btn_add_roi,1,0)
        b_layout.addWidget(self.btn_transform_rois,1,1)
        b_layout.addWidget(self.btn_reset_rois,1,2)
        b_layout.addWidget(self.btn_project,2,0)
        b_layout.addWidget(self.btn_save_image,8,1)
        b_layout.addWidget(self.tbl,3,0,5,2)
        #b_layout.addWidget(self.btn_record,8,0,1,3)
        b_layout.addWidget(self.btn_record,8,2)
        
        #prepare space for snapped frame
        self.scene = QtGui.QGraphicsScene()
        self.view  = QtGui.QGraphicsView(self.scene)
        vbox = QtGui.QVBoxLayout()
        vbox.addWidget(self.view)
        
        #set up main layout
        main_layout = QtGui.QGridLayout()
        main_layout.setColumnStretch(0,3)
        main_layout.setColumnStretch(3,8)
        
        #attach widgets and layouts
        main_layout.addLayout(b_layout,0,0)
        main_layout.addLayout(vbox,0,3)
        
        #transformation window
        self.transformwin=None
        #self.transformQ=False probably unnecessary
        self.selectedROIs = []
        self.translatemin = 1 #min unit of translation/moving
        self.rotatemin = 0.25 #min theta ang unit of rotation
        self.shearmin = 0.1 #min amount of shear
        self.rescalemin = 1.002 #min amount of rescale
        #initialize the camera
        p = optparse.OptionParser(usage="Usage: %prog [ options ]\n This program lets the camera run in free running mode.")
        add_common_options(p)
        options, args = p.parse_args()
        lib = DC1394Library()
        self.init_cam = handle_common_options(options,lib)
        
        self.setLayout(main_layout)
        self.show()
 
 
        #snap window
        self.snapcurpx = None



        #projection routines
        #self.win = None
        #if self.win !=None:
        #     del self.win
        self.win = PIMWin(1,False) #verbose/debugging info show set to False
        self.win.project_void()


    def __validate_files__(self):
        """Check for presence and validity of all dependencies"""
        if not os.path.exists("Outputfiles"):
            print "Creating Outputfiles sub dir...",
            os.mkdir("Outputfiles")
            print "Done!"
    
    def open_camera(self,pressed):
        source = self.sender()
        if not pressed:
            
            self.video_player = VideoPlayer(self.init_cam)
        else:
            print "camera closed"
        self.resetverts()


    def snapshot(self,pressed):
        source = self.sender()
        self.view.mousePressEvent = self.mousePressEvent#get_mouse_loc_on_press
        #print "pressed"
        self.resetverts()
        #self.regions = [] why is this a good idea?
        if not pressed:
            frame = np.asarray(self.video_player.myCam.frame[:,:])
            self.snapped_image = frame.copy()
            #cv2.imwrite('Outputfiles/snapped_frame.png',frame)
            #print "snapped image saved as Outputfiles/snapped_frame.png"
            h,w = frame.shape
            print h,w
            pix = QtGui.QPixmap(QtGui.QImage(frame.repeat(3).tostring(),int(w),int(h),QtGui.QImage.Format_RGB888).rgbSwapped()).scaled(640,480)#
            print "pix transform with success"
            self.scene.clear()
            print "scene cleared"
            self.scene.addPixmap(pix)
            print "pix added to scene"
            self.snapcurpx = pix




    def add_roi(self,pressed):
        self.regions.append(np.array(self.vertices,np.int32))
        print self.vertices, "added to regions of interest"
        print "number or regions:", len(self.regions)
        #maybe update self table here!
        self.vertices = []
    
    #def mousePressEvent(self,QMouseEvent):
     #   print QMouseEvent.pos()


    def mousePressEvent(self, QMouseEvent ):#get_mouse_loc_on_press
        position = QMouseEvent.pos()
        #position = (event.pos().x(), event.pos().y())
        #position = (event.pos().x() - self.view.pos().x(),
        #            event.pos().y() - self.view.pos().y())
        self.vertices.append([position.x()-50,position.y()-15]) #sometimes this works well, othertimes the clicks
                                                                #appear very far from the mouse
        #draw point
        #point = QtCore.QPointF(self.view.mapToScene(position))
        #line = QtCore.QLineF(point,point)
        #pen = QtGui.QPen(QtCore.Qt.red, 0, QtCore.Qt.SolidLine)
        #pen.setWidth(5)
        #self.view.scene().addLine(line,pen)
        #self.view.scene().addItem(QtGui.QGraphicsLineItem(QtCore.QLineF(point, point)))
        if self.snapcurpx!=None:
            self.clear_scene()
            self.draw_ROIs()
            self.draw_vertices()
        #print "click location:",(position.x(),position.y())
    
    def draw_ROIs(self):
        for i in range(len(self.regions)):
            for j in range(len(self.regions[i])):
                position = self.regions[i][j]
                #print position
                point = QtCore.QPointF(self.view.mapToScene(position[0],position[1]))
                line = QtCore.QLineF(point,point)
                pen = QtGui.QPen(QtCore.Qt.red, 0, QtCore.Qt.SolidLine)
                pen.setWidth(5)
                self.view.scene().addLine(line,pen)
                self.view.scene().addItem(QtGui.QGraphicsLineItem(QtCore.QLineF(point, point)))
    def draw_vertices(self):
        #self.clear_roisscene()
        for i in range(len(self.vertices)):
           # for j in range(len(self.regions[i])):
            position = self.vertices[i]
            #print position
            point = QtCore.QPointF(self.view.mapToScene(position[0],position[1]))
            line = QtCore.QLineF(point,point)
            pen = QtGui.QPen(QtCore.Qt.red, 0, QtCore.Qt.SolidLine)
            pen.setWidth(5)
            self.view.scene().addLine(line,pen)
            self.view.scene().addItem(QtGui.QGraphicsLineItem(QtCore.QLineF(point, point)))

    def transform_rois(self):
        #VideoPlayer(self.init_cam)
        self.transformwin = ROITransformWIN(len(self.regions))
        print 'in transform_rois'
        #self.transformQ=True

    def keyPressEvent(self, event): #eventually key pressing should not be tied entirely to ROI transformation; it is not modular and could be problematic
            if len(self.regions) == 0:
                return #do nothing as there are no regions to transform
            if self.transformwin == None:
                print ' You must open TransformROIs and choose which ROIs you wish to transform' 
                return 
            self.selectedROIs = self.transformwin.getSelected()
            #print 'key event with selected ROIS ' + str(self.selectedROIs) 
            #translation routines
            dx = 0
            dy = 0
            if(event.key()==QtCore.Qt.Key_W):
                #print 'up'
                dy = dy - self.translatemin      
            if(event.key()==QtCore.Qt.Key_A):
                #print 'left'  
                dx = dx - self.translatemin
            if(event.key()==QtCore.Qt.Key_S):
                #print 'down'  
                dy = dy + self.translatemin
            if(event.key()==QtCore.Qt.Key_D):
                #print 'right'  
                dx = dx + self.translatemin
            if dx != 0 or dy !=0:
                for i in range(len(self.selectedROIs)):
                    self.regions[self.selectedROIs[i]] = translate(self.regions[self.selectedROIs[i]],dx,dy)

            dtheta = 0
            if(event.key()==QtCore.Qt.Key_Q):
                dtheta = dtheta + self.rotatemin
            if(event.key()==QtCore.Qt.Key_E):
                dtheta = dtheta - self.rotatemin
            if dtheta != 0:
                for i in range(len(self.selectedROIs)):
                    self.regions[self.selectedROIs[i]] = rotate(self.regions[self.selectedROIs[i]],dtheta)

            sx = 0
            sy = 0
            if(event.key()==QtCore.Qt.Key_Z):
                sx = sx+self.shearmin      
            if(event.key()==QtCore.Qt.Key_X):
                sx  = sx-self.shearmin
            if(event.key()==QtCore.Qt.Key_C):  
                sy = sy+self.shearmin
            if(event.key()==QtCore.Qt.Key_V):
                sy = sy-self.shearmin
            #print 'sx: '+str(sx)+ ' sy: '+str(sy)
            if sx!=0 or sy!=0:#sx != 1 or sy != 1:
                for i in range(len(self.selectedROIs)):
                    self.regions[self.selectedROIs[i]] = unitaryrescale(self.regions[self.selectedROIs[i]],sx,sy)

            cx = 1
            cy = 1
            if(event.key()==QtCore.Qt.Key_B):
                cx = cx*self.rescalemin*1.25 #for some reason the increase functions is are substantially     
            if(event.key()==QtCore.Qt.Key_N): #more sensitive than the decrease ones :/ what is life even
                cx  = cx/self.rescalemin
            if(event.key()==QtCore.Qt.Key_M):  
                cy = cy*self.rescalemin*1.25
            if(event.key()==QtCore.Qt.Key_Comma):
                cy = cy/self.rescalemin
            if(event.key()==QtCore.Qt.Key_Equal):
                cx = cx*self.rescalemin*1.25 
                cy = cy*self.rescalemin*1.25
            if(event.key()==QtCore.Qt.Key_Minus):
                cx = cx/self.rescalemin 
                cy = cy/self.rescalemin
            if cx != 1 or cy != 1:
                for i in range(len(self.selectedROIs)):
                    self.regions[self.selectedROIs[i]] = rescale(self.regions[self.selectedROIs[i]],cx,cy)
            #print self.regions
            if self.snapcurpx!=None:
                self.clear_scene()
                self.draw_ROIs()
                self.draw_vertices()

                
    def clear_scene(self):
        self.scene.clear()
        self.scene.addPixmap(self.snapcurpx)

    def reset_rois(self):
        self.vertices=[]
        self.regions=[]
        self.clear_scene()
        print 'All locally saved roi info reset.'

    def project(self,pressed):#static project
        source = self.sender()
        if not pressed:
            try:
                if self.snapped_image != None:
                    imcopyarr = self.snapped_image.copy() #an array of pixels
                    cv2.fillPoly(imcopyarr, self.regions, 1)
                    cv2.imwrite("Outputfiles/projection.png",imcopyarr)
                    #print type(imcopyarr)
                    print "projection saved on disc"
                    
                    #display projected image/ This is for debugging. It should be removed in the final version
                    frame = imcopyarr.copy()
                    #print np.amax(frame)
                    #print np.amin(frame)
                    #time.sleep(0.5)
                    h,w = frame.shape
                    pix = QtGui.QPixmap(QtGui.QImage(frame.repeat(3).tostring(),int(w),int(h),QtGui.QImage.Format_RGB888).rgbSwapped()).scaled(640,480)
                    self.scene.clear()
                    self.scene.addPixmap(pix)
                    def monotoRGBI(mirow):#ith mono px row
                        return map(lambda mpx:[255,255,255,float(mpx)/float(255)],mirow)
                    imcopyarr = np.ones((self.snapped_image.copy()).shape)*75 #an array of black pixels
                    cv2.fillPoly(imcopyarr, self.regions, 1)
                    RGBpxarr=arr(map(lambda monopxrow:monotoRGBI(monopxrow),imcopyarr.tolist()))

                    (self.win).project_const(RGBpxarr,1.0)
                else:
                    print "snapped image and select ROIs first"
            except FutureWarning: #
            #prevent this annoying/useless error from showing up in terminal
                print 'error caught'
                l=0 #i.e. do nothing 
            else:
                l=0 #i.e. do nothing
   
                #os.system("python project_img_gen3mainloop.py 1")#replace this with new projection code
                               #imcopyarr = np.ones((self.snapped_image.copy()).shape)*255 #an array of pixels


        self.resetverts()


    def record(self):
        source = self.sender()
        if not self._record_signal:
            print "recording ..."
            self._record_signal = True
            self.frame_counter = 0
            self.btn_record.setText('Stop')
            self.timer = QtCore.QTimer()
            self.timer.setInterval(int(self.time_interval*1000))
            self.timer.timeout.connect(self._save_image_)
            self.timer.start()

        else:
            self.timer.stop()
            self.btn_record.setText('Record')
            self._record_signal = False
            print "done recording!"
        self.resetverts()

    def _save_image_(self):
        self.frame_counter+=1
        frame = np.asarray(self.video_player.myCam.frame[:,:])
        filename = "Outputfiles/img_{0:04}.png".format(self.frame_counter)
        cv2.imwrite(filename,frame)
        print "frame saved as", filename
        self.resetverts()

    def save_snapped(self):
        if self.snapped_image != None:
            cv2.imwrite("Outputfiles/snapped_frame.png",self.snapped_image)
            print "image saved as Outputfiles/snapped_image.png"
        else:
            print "None snapped image"
        self.resetverts()

    def closeEvent(self,event):
        self.video_player.close()
        print "PIM App closed successfully."
        quit()

    #def trackROIs: ADD DYNAMIC DETECTION HERE
    def resetverts(self):
        self.vertices = []


class ROITransformWIN(QtGui.QWidget):
    def __init__(self,numROIs):
        #print 'entered ROITransformWIN initiation with numROIs '+str(numROIs)
        QtGui.QWidget.__init__(self)
            #layout = QVBoxLayout()
        layout = QtGui.QGridLayout()
        self.checks = []
        self.checked = []
        self.numroi=numROIs
        for i in xrange(self.numroi):
            c = QtGui.QCheckBox("ROI %i" % i)
            layout.addWidget(c,0,i)
            self.checks.append(c)
            self.checks[len(self.checks)-1].clicked[bool].connect(self.ROIselectionchange)
        textbox=QtGui.QPushButton('Transforming ROIS: \n \
                    apply face directly to keyboard i.e.   \n \
                    Select ROIs you want to transform with the checkbox above \n \
                    Then reselect the original PIM window and use the follow keyboard commands to transform the ROIs \n \
                    Move: use W,S,D,A for up, down, right, left \n \
                    Rotate: use Q,E for CW or CCW rotation \n \
                    Rescale uniform: use "-" and "=" for dx=dy increase or decrease in size of ROIs \n \
                    Unitary rescale (shear): Z,X for +,- x axis or C,V for +,- y axis shear about COM \n \
                    Rescale: B,N for +,- x axis or M,"," for +,- y axis rescale about COM \n \
                    If you add more ROIs, close and reopen this window')
        layout.addWidget(textbox,1,0,5,0)
        self.setLayout(layout)
        self.show()

        #def changeEvent(self,QMouseEvent):
    def ROIselectionchange(self):
        #print 'hello'
        print '\n'
        self.checked=[]
        for i in range(self.numroi):
            if (self.checks[i]).isChecked():
                print "ROI " + str(i) + " is selected"
                self.checked.append(i)
        if len(self.checked)==0:
            print 'No ROIs selected'
    def getSelected(self):
            return self.checked
def main():
    app = QtGui.QApplication(sys.argv)
    mainWindow = MainWindow()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
