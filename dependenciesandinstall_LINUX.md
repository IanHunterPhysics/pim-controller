Full PIM for firewire DCAM:
	$sudo apt-get install libopencv-dev python-opencv
	$sudo apt-get install build-essential cmake pkg-config libpng12-0 libpng12-dev
	$sudo apt-get install libpng3 libpnglite-dev ibpngwriter0-dev libpngwriter0c2
	$sudo apt-get install zlib1g-dbg zlib1g zlib1g-dev libjasper-dev 
	$sudo apt-get install libjasper1 pngtools libtiff4-dev libtiff4 libtiffxx0c2 
	$sudo apt-get install libjpeg8 libjpeg8-dev libjpeg8-dbg libjpeg-prog ffmpeg
	$sudo apt-get install libavcodec-dev libavcodec52 libavformat52 libavformat-dev
	$sudo apt-get install libgstreamer0.10-0-dbg libgstreamer0.10-0
	$sudo apt-get install libxine1-ffmpeg libxine-dev libxine1-bin
	$sudo apt-get install libdc1394-22-dev libdc1394-22 libdc1394-utils
	$sudo apt-get install swig libv4l-0 libunicap2 libunicap2-dev
	$sudo apt-get install libgstreamer0.10-dev libtiff-tools
	$sudo apt-get install libjasper-runtime libv4l-dev python-numpy
	$sudo apt-get install libpython2.6 python-dev 
	$sudo pip install git+https://github.com/imrehg/pydc1394
	$sudo easy_install pyopengl
	$sudo apt-get install python-qt4-gl




You can run all of these by copying and pasting the following into your terminal:

sudo apt-get install libopencv-dev python-opencv && sudo apt-get install build-essential cmake pkg-config libpng12-0 libpng12-dev && sudo apt-get install libpng3 libpnglite-dev ibpngwriter0-dev libpngwriter0c2 && sudo apt-get install zlib1g-dbg zlib1g zlib1g-dev libjasper-dev && sudo apt-get install libjasper1 pngtools libtiff4-dev libtiff4 libtiffxx0c2 && sudo apt-get install libjpeg8 libjpeg8-dev libjpeg8-dbg libjpeg-prog ffmpeg && sudo apt-get install libavcodec-dev libavcodec52 libavformat52 libavformat-dev && sudo apt-get install libgstreamer0.10-0-dbg libgstreamer0.10-0 && sudo apt-get install libxine1-ffmpeg libxine-dev libxine1-bin && sudo apt-get install libdc1394-22-dev libdc1394-22 libdc1394-utils && sudo apt-get install swig libv4l-0 libunicap2 libunicap2-dev && sudo apt-get install libgstreamer0.10-dev libtiff-tools && sudo apt-get install libjasper-runtime libv4l-dev python-numpy && sudo apt-get install libpython2.6 python-dev && sudo pip install git+https://github.com/imrehg/pydc1394 && sudo easy_install pyopengl && sudo apt-get install python-qt4-gl
